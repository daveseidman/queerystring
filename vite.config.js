import path from 'path';
import { defineConfig } from 'vite';

export default defineConfig({
  server: { port: 8080 },
  devSourcemap: true,
  build: {
    outDir: 'public',
  },
});
