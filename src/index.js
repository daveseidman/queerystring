import autoBind from 'auto-bind';

const SPEED = 200;
const VARIATION = 0.01;

const answers = {
  blue: "oh, you're asking about blue things",
  red: 'ah, you want to know about red stuff',
  green: 'sure, we can talk about green for a bit',
  yellow: 'lets talk about yellow stuff',
};
class App {
  constructor() {
    autoBind(this);

    this.computerTyping = false;
    this.state = {
      computerTyping: false,
      userTyping: false,
      currentHash: '',
      previousHash: '',
      stopRequested: false,
    };

    addEventListener('focus', this.userInput);
    addEventListener('blur', this.stopTyping);
  }

  // checkHash() {
  //   console.log('checking has', this.state.currentHash);
  //   this.state.currentHash = location.hash;
  //   if (this.state.currentHash !== this.state.previousHash) {
  //     console.log('hash changed:', this.state.currentHash.slice(1).split('%20').join(' '));
  //   }
  //   this.state.previousHash = this.state.currentHash;
  // }

  userInput() {
    const cleanInput = location.hash.slice(1).split('%20').join(' ');
    console.log('user says', cleanInput);
    let match = '';
    Object.keys(answers).forEach((key) => {
      if (cleanInput.indexOf(key) >= 0) match = answers[key];
    });
    if (match.length) {
      this.continueTyping(match);
    } else {
      console.log('no match');
    }
  }

  typeCharacter(phrase) {
    return new Promise((resolve) => {
      setTimeout(() => {
        const character = phrase.slice(0, 1).replace(' ', '_');
        const rest = phrase.slice(1, phrase.length);
        location.hash += character;
        resolve(rest);
      }, (Math.random() + VARIATION) * SPEED);
    });
  }

  continueTyping(phrase) {
    return new Promise((resolve) => {
      if (phrase.length === 0) return resolve();
      if (this.state.stopRequested) {
        this.state.stopRequested = false;
        return resolve();
      }
      this.typeCharacter(phrase).then(this.continueTyping);
    });
  }

  stopTyping() {
    this.state.stopRequested = true;
  }

  clearHash() {
    location.hash = '';
  }

  clearPhrase() {

  }
}

const app = new App();
if (location.hostname === 'localhost') window.app = app;
